from segmentation import LDA, QDA, KNeighborsClassifier, tree, svm, GaussianProcessRegressor, DotProduct
from segmentation import WhiteKernel, Enum,os,pickle, cv2


class MlClassifier(Enum):
    """
    Classifier Objects
    """
    # LDA = LDA()
    # QDA = QDA()
    #KNN = KNeighborsClassifier(n_neighbors=5)
    DecisionTree = tree.DecisionTreeClassifier()
    # SVM = svm.SVC()
    # GPR = GaussianProcessRegressor(kernel=DotProduct() + WhiteKernel(), random_state=0)

    # for neural net:
    # https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
    # NEURAL_NET = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)

    def train(self, intensities_tot, classes_tot):
        return self.value.fit(intensities_tot, classes_tot)



    # def _load_clf(self):
    #     path = os.path.join('O:\\Shai\\projects\\mask_project', str(self.classifier), 'Train',
    #                         self.pre_action.__name__ + '_' + self.post_action.__name__,
    #                         self.color_space.name, 'finalized_model.sav')
    #     loaded_model = pickle.load(open(path, 'rb'))
    #     return loaded_model


# class ClassifierModel:
#
#     def __init__(self):
#         self._score = None
#         self._name = ""
#
#     @property
#     def score(self):
#         return self._score
#
#     @score.setter
#     def score(self, value):
#         self._score = value
#
#     @property
#     def name(self):
#         return self._name
#
#     @name.setter
#     def name(self, name_val):
#         self._name = name_val
#
#     def fit(self):
#         pass
#
#     def predict(self):
#         pass
#
# class OtsuThreshold(ClassifierModel):
#
#     def __init__(self):
#         super().__init__()
#         self.name = "OtsuThreshold"
#
#     def fit(self, X, Y):
#         return None
#
#     def predict(self):
#         cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
#
