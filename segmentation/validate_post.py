# ###### from segmentation #####
# BASE_PATH = "O:\\Shai\\projects\\mask_project"
from segmentation import os, BASE_PATH, PostprocessingActionsOptions, cv2, plt, GT_CH, PRED_CH, MASK_CH, save_img
from segmentation import split_image

_PRED_IMAGE = '_predict_image.png'
pred_len = (len('_predict_image.png'))


class ValidatePost:
    pass

    # read pred image

    def read_files(self):
        image_holders_list = list()
        for subdir, dirs, files in os.walk(os.path.join(BASE_PATH)):
            for file in files:
                if file.endswith(_PRED_IMAGE):
                    debug_image = cv2.imread(os.path.join(subdir, file))
                    gt_im, pred_im, mask_im = split_image(debug_image)
                    color = file[:-(len(_PRED_IMAGE) + 2)]  # +2 to remove image number
                    color_space = subdir.split('\\')[-1]
                    pre_processing, post_action = (subdir.split('\\')[-2]).split('-')
                    clf = subdir.split('\\')[-4]
                    # iter on post
                    for post_action in PostprocessingActionsOptions:
                        im_post = self.post_process_image(pred_im, post_action)
                        path = os.path.join(BASE_PATH, 'validate_post', clf, post_action.name, color_space)
                        save_img(path, im_post, "after_post_" + color)
                        # plt.imshow(im_post)
                        # plt.show()

    # score in + out

    @staticmethod
    def post_process_image(image, post_process):
        return post_process.get_func()(image)


if __name__ == '__main__':
    ValidatePost().read_files()
