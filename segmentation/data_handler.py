from segmentation import ImageHolder, typing, ColorSpace, PreprocessingActionsOptions
from segmentation import np, PolishColor, glob, os, cv2, plt, print_progress
from segmentation import GT_, PNG, my_print, PRE_WITHOUT_, PRE_WITH_, MASK_, DEBUF_PATH, save_img, BASE_PATH

_TRAIN_DATA = "TrainData"
_TEST_DATA = "TestData"


class DataReader:

    def __init__(self, directory):
        self.path = directory

    def read_data(self, load_train: bool = True, load_test: bool = True):
        # get all files in dir
        # iterate on file:
        # for file (color) fg check if all file exist and create image holder object
        # check if train or test

        if load_train:
            image_for_train = self.get_files_from_dir(os.path.join(self.path, _TRAIN_DATA))

        if load_test:
            image_for_test = self.get_files_from_dir(os.path.join(self.path, _TEST_DATA))

        return DataHolder(image_for_train, image_for_test)

    def get_files_from_dir(self, directory):
        image_holders_list = list()
        for file in os.listdir(directory):
            if file.startswith(GT_):
                color_name = file[len(GT_):-len(PNG)]
                is_exist, gt_img, mask, bg_img, fg_img = self.get_images_if_all_exist(directory, color_name)
                if is_exist:  # all exist:
                    print_progress("#")
                    image_holder = ImageHolder(name=color_name,
                                               fg_img=fg_img,
                                               bg_img=bg_img,
                                               gt_segment=gt_img,
                                               mask=mask)
                    image_holders_list.append(image_holder)
                else:
                    my_print(f"Not all files exist for {color_name}!")
        my_print("\n")
        return image_holders_list

    @staticmethod
    def get_images_if_all_exist(directory, color_name):
        is_exist = False
        gt_path = os.path.join(directory, GT_ + color_name + PNG)
        mask_path = os.path.join(directory, MASK_ + color_name + PNG)
        bg_path = os.path.join(directory, PRE_WITHOUT_ + color_name + PNG)
        fg_path = os.path.join(directory, PRE_WITH_ + color_name + PNG)

        gt_img = cv2.imread(gt_path, 0)
        mask = cv2.imread(mask_path, 0)
        bg_img = cv2.imread(bg_path)
        fg_img = cv2.imread(fg_path)

        if gt_img is not None and mask is not None and bg_img is not None and fg_img is not None:
            is_exist = True

        return is_exist, gt_img, mask, bg_img, fg_img


class DataHolder:
    def __init__(self, images_for_train: typing.List[ImageHolder] = None,
                 images_for_test: typing.List[ImageHolder] = None):
        self._train_images = images_for_train if images_for_train is not None else list()
        self._test_images = images_for_test if images_for_test is not None else list()

    @property
    def train_images(self):
        return self._train_images

    @property
    def test_images(self):
        return self._test_images

    def add_train_image_holder(self, image_holder):
        self._train_images.append(image_holder)

    def add_test_image_holder(self, image_holder):
        self._test_images.append(image_holder)

    def get_train_data(self):
        return self._train_images

    def get_test_data(self):
        return self._test_images

    def get_data_for_train(self, train_options):
        # I1, I2, I3, Class
        intensities, classes = dict(), dict()
        my_print("start get_data_for_train")
        for image_holder in self._train_images:
            proc_im = image_holder.pre_process_image(train_options.color_space, train_options.pre_process)
            path = os.path.join(BASE_PATH, train_options.classifier.name, 'Train',
                                train_options.pre_process.name + "-" + train_options.post_process.name,
                                train_options.color_space.name)
            save_img(path=path,
                     img=proc_im,
                     title=image_holder.name + "_1_proc_im")
            pre_proc_intensities = image_holder.get_data_in_mask(proc_im)
            gt_data = image_holder.get_data_in_mask(image_holder.gt_segment)
            intensities[image_holder.name] = pre_proc_intensities
            gt_data[gt_data > 0] = 1
            classes[image_holder.name] = gt_data
        my_print("processed images saved")
        return np.vstack(list(intensities.values())), np.hstack(list(classes.values())), intensities, classes

    def get_data_for_test(self, train_options):
        # I1, I2, I3, Class
        intensities, classes = dict(), dict()
        my_print("start get_data_for_test")
        for image_holder in self._test_images:
            proc_im = image_holder.pre_process_image(train_options.color_space, train_options.pre_process)
            path = os.path.join(BASE_PATH, train_options.classifier.name, 'Test',
                                train_options.pre_process.name + "-" + train_options.post_process.name,
                                train_options.color_space.name)
            save_img(path=path,
                     img=proc_im,
                     title=image_holder.name + "_1_proc_im")
            pre_proc_intensities = image_holder.get_data_in_mask(proc_im)
            gt_data = image_holder.get_data_in_mask(image_holder.gt_segment)
            intensities[image_holder.name] = pre_proc_intensities
            gt_data[gt_data > 0] = 1
            classes[image_holder.name] = gt_data
        my_print("processed images saved")
        return intensities, classes
