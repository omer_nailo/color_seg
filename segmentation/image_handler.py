from segmentation import Enum, cv2, np, convex_hull_image, white_tophat, black_tophat


class ColorSpace(Enum):
    RGB = cv2.COLOR_BGR2RGB
    YCrCb = cv2.COLOR_BGR2YCrCb
    # HSV = cv2.COLOR_BGR2HSV
    # HLS = cv2.COLOR_BGR2HLS
    # LAB = cv2.COLOR_BGR2LAB
    # LUV = cv2.COLOR_BGR2LUV

    def convert(self, im):
        return cv2.cvtColor(im, self.value)


class PreprocessingActions:
    """
    Actions are difference between two images.
    return np.array
    """

    @staticmethod
    def subtract(im_fg: np.ndarray, im_bg: np.ndarray) -> np.ndarray:
        # cv2.absdiff: finding the absolute difference between the pixels of the two image arrays
        return cv2.absdiff(im_fg, im_bg)

    @staticmethod
    def gauss_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        # cv2.GaussianBlur: blur an image
        return PreprocessingActions.subtract(cv2.GaussianBlur(im_fg, (15, 15), 9), cv2.GaussianBlur(im_bg, (15, 15), 9))

    @staticmethod
    def median_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.medianBlur: computes the median of all the pixels under the kernel window
        and the central pixel is replaced with this median value.
        """
        return PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))

    @staticmethod
    def laplace_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.Laplacian: used to find edges in an image
        """
        fg = cv2.merge([cv2.Laplacian(ch, cv2.CV_8U) for ch in cv2.split(im_fg)])
        bg = cv2.merge([cv2.Laplacian(ch, cv2.CV_8U) for ch in cv2.split(im_bg)])
        return PreprocessingActions.subtract(fg, bg)

    @staticmethod
    def sub_and_laplace(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        return cv2.merge(
            [cv2.Laplacian(ch, cv2.CV_8U) for ch in cv2.split(PreprocessingActions.subtract(im_fg, im_bg))])

    @staticmethod
    def sobel_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.Sobel: computes an approximation of the gradient of an image intensity function
        """
        fg = cv2.merge([cv2.Sobel(ch, cv2.CV_8U, 1, 1) for ch in cv2.split(im_fg)])
        bg = cv2.merge([cv2.Sobel(ch, cv2.CV_8U, 1, 1) for ch in cv2.split(im_bg)])
        return PreprocessingActions.subtract(fg, bg)

    @staticmethod
    def scharr_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.Scharr: Scharr kernel may give us better approximations to the gradient then Sobel kernel
        """
        fg = cv2.merge([cv2.Scharr(ch, cv2.CV_8U, 1, 0) for ch in cv2.split(im_fg)])
        bg = cv2.merge([cv2.Scharr(ch, cv2.CV_8U, 1, 0) for ch in cv2.split(im_bg)])
        return PreprocessingActions.subtract(fg, bg)

    @staticmethod
    def hist_eq_cutoff(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        res = PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))
        return np.uint8(255 * (cv2.merge([cv2.equalizeHist(ch) for ch in cv2.split(res)]) > 240))

    @staticmethod
    def hist_eq(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.equalizeHist: contrast adjustment using the image's histogram.
        useful in images with backgrounds and foregrounds that are both bright or both dark
        """
        res = PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))
        return np.uint8(255 * (cv2.merge([cv2.equalizeHist(ch) for ch in cv2.split(res)])))

    @staticmethod
    def hist_eq_median(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        res = PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))
        res = np.uint8(255 * (cv2.merge([cv2.equalizeHist(ch) for ch in cv2.split(res)])))
        return cv2.medianBlur(res, 5)

    @staticmethod
    def subtract_and_normalize(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.normalize: changing the pixel intensity and increasing the overall contrast
        """
        diff_img = PreprocessingActions.subtract(im_fg, im_bg)
        norm_img = np.zeros_like(diff_img)
        return cv2.normalize(diff_img, norm_img, 0, 255, cv2.NORM_MINMAX)


class PreprocessingActionsOptions(Enum):
    """
    Value is the function from Actions
    """
    # SUBTRACT = (PreprocessingActions.subtract,)
    HIST_EQ = (PreprocessingActions.hist_eq,)

    # SUBTRACT_AND_NORMALIZE = (PreprocessingActions.subtract_and_normalize,)
    # GAUSS_AND_SUB = (PreprocessingActions.gauss_and_sub,)
    # MEDIAN_AND_SUB = (PreprocessingActions.median_and_sub,)
    # LAPLACE_AND_SUB = (PreprocessingActions.laplace_and_sub,)
    # SUB_AND_LAPLACE = (PreprocessingActions.sub_and_laplace,)
    # SOBEL_AND_SUB = (PreprocessingActions.sobel_and_sub,)
    # SCHARR_AND_SUB = (PreprocessingActions.scharr_and_sub,)
    # HIST_EQ_CUTOFF = (PreprocessingActions.hist_eq_cutoff,)
    # HIST_EQ_MEDIAN = (PreprocessingActions.hist_eq_median,)

    def get_func(self):
        return self.value[0]

    def process(self, fg_im, bg_im):
        return self.get_func()(fg_im, bg_im)


class PostprocessingActions:
    @staticmethod
    def post_process_morph(image: np.array) -> np.array:
        im_open = cv2.morphologyEx(image, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15)))
        im_close = cv2.morphologyEx(im_open, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
        return PostprocessingActions._make_convex(im_close)

    @staticmethod
    def post_process_median(image: np.array) -> np.array:
        im_median = cv2.medianBlur(image, 15)
        return PostprocessingActions._make_convex(im_median)

    @staticmethod
    def close_median(image, kernel_size=7):
        im_close = cv2.morphologyEx(image, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
        im_median = cv2.medianBlur(im_close, kernel_size)
        return PostprocessingActions._make_convex(im_median)

    @staticmethod
    def close_convex(image: np.array):
        im_close = cv2.morphologyEx(image, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15)))
        return PostprocessingActions._make_convex(im_close)

    @staticmethod
    def close_open_median_convex(image: np.array):
        im_median = cv2.medianBlur(image, 9)
        im_close = cv2.morphologyEx(im_median, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
        im_open = cv2.morphologyEx(im_close, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))

        return PostprocessingActions._make_convex(im_open)

    @staticmethod
    def white_hat(image: np.array):
        return PostprocessingActions._make_convex(white_tophat(image))

    @staticmethod
    def black_hat(image: np.array):
        return PostprocessingActions._make_convex(black_tophat(image))

    # @staticmethod
    # def nothing(image: np.array):
    #     return np.uint8(255 * image)

    @staticmethod
    def _make_convex(image: np.array, area_thresh: int = 100) -> np.array:
        """

        Args:
            image: binary image for convex hull
            area_thresh:

        Returns:

        """
        # contours = cv2.findContours(np.array(image).astype(np.uint8),
        #                             cv2.RETR_EXTERNAL,
        #                             cv2.CHAIN_APPROX_SIMPLE)[-2]
        if image.ndim == 3:
            image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        contours = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

        # convex_contour = [cv2.convexHull(cnt, False) for cnt in contours if cv2.contourArea(cnt) > area_thresh]

        contours_filters = [cnt for cnt in contours if cv2.contourArea(cnt) > area_thresh]
        convex_image = cv2.drawContours(np.zeros_like(image, dtype=np.uint8), contours_filters, -1, 255, -1)
        # from skimage.morphology import convex_hull_image
        return np.uint8(255 * convex_hull_image(convex_image))
        # return cv2.drawContours(np.zeros_like(image, dtype=np.uint8), convex_contour, -1, 255, -1)


class PostprocessingActionsOptions(Enum):
    """
    Value is the function from PostprocessingActions
    """
    # MORPH = (PostprocessingActions.post_process_morph,)
    # MEDIAN = (PostprocessingActions.post_process_median,)
    CLOSE_MEDIAN = (PostprocessingActions.close_median,)

    # CLOSE_CONVEX = (PostprocessingActions.close_convex,)
    # CLOSE_OPEN_MEDIAN_CONVEX = (PostprocessingActions.close_open_median_convex,)
    # NOTHING = (PostprocessingActions.nothing,)

    def get_func(self):
        return self.value[0]


class ImageHolder:
    def __init__(self, name, fg_img, bg_img, gt_segment, mask: np.array = None):
        self._name = name
        self._bg_img = bg_img
        self._fg_img = fg_img
        self._im_shape = fg_img.shape[:2]
        self._gt_segment = gt_segment
        self._mask_img = mask if mask is not None else np.ones(self._im_shape)
        self._mask_area = np.count_nonzero(self._mask_img)

    @property
    def bg_img(self):
        return self._bg_img.copy()

    @property
    def fg_img(self):
        return self._fg_img.copy()

    @property
    def gt_segment(self):
        return self._gt_segment.copy()

    @property
    def mask(self):
        return self._mask_img.copy()

    @property
    def name(self):
        return self._name

    def from_array_to_im(self, data: np.array, dtype=None):
        dtype_im = dtype if dtype else data.dtype
        data_dim = data.ndim
        if data_dim == 1:
            im = np.zeros(self._im_shape)
        if data_dim == 2 and data.shape[-1] == 3:
            im = np.zeros(self._im_shape + (3,))
        if data.shape[0] == self._mask_area:
            im[self.mask > 0] = data
            return np.array(im, dtype_im)
        else:
            raise ValueError(
                f"data shape{data.shape} and mask shape {self._mask_area} are not the same for color {self.name}")

    def put_image_on_fg(self, image):
        image_to_add = image.copy()
        if image_to_add.ndim == 2:
            image_to_add = cv2.cvtColor(image_to_add, cv2.COLOR_GRAY2BGR)
        self.weighted = cv2.addWeighted(self.fg_img, 0.7, image_to_add, 0.3, 0)
        return self.weighted

    def put_data_on_fg(self, data):
        return self.put_image_on_fg(self.from_array_to_im(data))

    def pre_process_image(self, color_space: ColorSpace, pre_proc: PreprocessingActionsOptions):
        fg_input, bg_input = self.mask_image(self._fg_img.copy()), self.mask_image(self._bg_img.copy())
        return pre_proc.process(color_space.convert(fg_input), color_space.convert(bg_input))

    def get_data_in_mask(self, im):
        return im[self.mask > 0]

    def mask_image(self, im):
        im[self.mask == 0] = 0
        return im
