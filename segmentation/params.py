from segmentation import os, cv2, np

BASE_PATH = "O:\\Shai\\projects\\mask_project"
PRE_WITHOUT_ = "pre_without_"
PRE_WITH_ = "pre_with_"
GT_ = "gt_"
MASK_ = "mask_"
PNG = ".png"
DEBUF_PATH = os.path.join(BASE_PATH, "DebugImage")

_DEBUG = True
_DEEP_DEBUG = False

GT_CH, PRED_CH, MASK_CH = 0, 1, 2


def my_print(*args, **kwargs):
    if _DEBUG:
        print(*args, **kwargs)


def deep_debug(*args, **kwargs):
    if _DEEP_DEBUG:
        print(*args, **kwargs)


def save_img(path, img, title):
    is_exist = os.path.exists(path)
    if not is_exist:
        os.makedirs(path)
    cv2.imwrite(os.path.join(path, title + PNG), img)
    deep_debug(f"{title} image saved")


def print_progress(msg: str):
    print(msg, end="", flush=True)


def split_image(image):
    im_split = cv2.split(image)
    gt_im = im_split[GT_CH]
    pred_im = im_split[PRED_CH]
    mask_im = im_split[MASK_CH]
    return gt_im, pred_im, mask_im
