from segmentation import PreprocessingActions, PostprocessingActions, MlClassifier, ColorSpace, PolishColor
from segmentation import PreprocessingActionsOptions, PostprocessingActionsOptions, DataHolder, DataReader
from segmentation import np, my_print, BASE_PATH, plt, save_img, DEBUF_PATH, os, ImageHolder, cv2, pickle, csv
from segmentation import GT_CH, PRED_CH, MASK_CH, time


class TrainerOptions:
    def __init__(self,
                 pre_process: PreprocessingActionsOptions,
                 classifier: MlClassifier,
                 post_process: PostprocessingActionsOptions,
                 color_space: ColorSpace):
        self._pre_process = pre_process
        self._classifier = classifier
        self._post_process = post_process
        self._color_space = color_space

    @property
    def pre_process(self):
        return self._pre_process

    @property
    def classifier(self):
        return self._classifier

    @property
    def post_process(self):
        return self._post_process

    @property
    def color_space(self):
        return self._color_space

    def to_names_list(self):
        return [self.pre_process.name, self.classifier.name, self.post_process.name, self.color_space.name]


class TrainScore:
    def __init__(self):
        self.classifier_score = None
        self.score_in = None
        self.score_out = None
        self.score_in_post = None
        self.score_out_post = None

    def set_scores(self, cls_score, score_in, score_out, score_in_post, score_out_post):
        self.classifier_score = cls_score
        self.score_in = score_in
        self.score_out = score_out
        self.score_in_post = score_in_post
        self.score_out_post = score_out_post

    def to_list(self):
        return [self.classifier_score, self.score_in, self.score_out, self.score_in_post, self.score_out_post]


class Trainer:
    def __init__(self, all_data: DataHolder, train_options: TrainerOptions):
        self.data = all_data
        # self._train_data = all_data.get_train_data()
        # self._test_data = all_data.get_test_data()
        self._train_options = train_options
        self._is_trained = False
        self._trained_classifier = None
        self._train_scores = None
        self.tot_score = None
        self._test_scores = None

    def train(self, train_writer):
        # handle_data - get X, Y
        intensities_tot, classes_tot, intensities, classes = self._get_data_for_train(self._train_options)
        self._inner_train(intensities_tot, classes_tot)
        # processed score
        self.tot_score = self._calc_tot_score(intensities_tot, classes_tot)
        self._train_scores = self.train_calc_scores_per_color(intensities, classes, train_writer, )

        # self._write_score_to_csv(train_writer,intensities)

    def _inner_train(self, intensities_tot, classes_tot):
        self._trained_classifier = self._train_options.classifier.train(intensities_tot, classes_tot)
        self._is_trained = True
        my_print("classifier is trained!")
        # save clf
        path = os.path.join(BASE_PATH, self._train_options.classifier.name, 'Train',
                            self._train_options.pre_process.name + "-" + self._train_options.post_process.name,
                            self._train_options.color_space.name)
        self._save_classifier(path)

    def test(self, test_writer):
        # handle_data - get X, Y
        intensities, classes = self._get_data_for_test(self._train_options)
        # processed score
        self._test_scores = self.test_calc_scores_per_color(intensities, classes, test_writer)

    def get_scores(self):
        if self._train_scores is None:
            self._calc_scores_per_color()
        return self._train_scores.to_list()

    def _calc_tot_score(self, intensities_tot, classes_tot):
        score_tot = self._trained_classifier.score(intensities_tot, classes_tot)
        return round(score_tot, 3)

    def test_calc_scores_per_color(self, intensities, classes, writer):
        path = os.path.join(BASE_PATH, self._train_options.classifier.name, 'Test',
                            self._train_options.pre_process.name + "-" + self._train_options.post_process.name,
                            self._train_options.color_space.name)
        return self._calc_scores_per_color(intensities, classes, self.data.test_images, writer,
                                           self._test_write_score_to_csv, path)

    def train_calc_scores_per_color(self, intensities, classes, writer):
        path = os.path.join(BASE_PATH, self._train_options.classifier.name, 'Train',
                            self._train_options.pre_process.name + "-" + self._train_options.post_process.name,
                            self._train_options.color_space.name)
        return self._calc_scores_per_color(intensities, classes, self.data.train_images, writer,
                                           self._write_score_to_csv, path)

    def _calc_scores_per_color(self, intensities, classes, images, writer, write_func, path):
        if not self._is_trained:
            raise BrokenPipeError("Classifier is not trained")

        scores = list()
        my_print(f"start saving predict images ewith {write_func}")
        for i, color in enumerate(intensities):
            my_print(f"calc proc score: {color}")
            pred_start_time = time.perf_counter()
            classes_prediction = self._trained_classifier.predict(intensities[color])
            pred_end_time = time.perf_counter()
            pred_tot = pred_end_time - pred_start_time
            my_print(f" t0: {pred_start_time} t1: {pred_end_time} pred tot:{pred_tot:0.4f} seconds")

            classes_gt = classes[color]
            proc_score_inside = self._calc_score_inside(classes_prediction, classes_gt)

            # Score Outside
            proc_score_outside = self._calc_score_outside(classes_prediction, classes_gt)

            im_post, post_tot = self._debug_im_prediction(image_holder=images[i],
                                                          path=path,
                                                          classes_prediction=classes_prediction,
                                                          classes_gt=classes_gt,
                                                          score_in=proc_score_inside,
                                                          score_out=proc_score_outside)
            my_print(f"calc post score: {color}")
            classes_after_post = images[i].get_data_in_mask(im_post)
            classes_after_post[classes_after_post == 255] = 1
            post_score_inside = self._calc_score_inside(classes_after_post, classes_gt)
            post_score_outside = self._calc_score_outside(classes_after_post, classes_gt)

            scores.append((
                color, round(proc_score_inside, 3), round(proc_score_outside, 3), round(post_score_inside, 3),
                round(post_score_outside, 3)))

            write_func(writer, color, round(proc_score_inside, 3), round(proc_score_outside, 3),
                       round(post_score_inside, 3), round(post_score_outside, 3), round(pred_tot, 4),
                       round(post_tot, 4))

    def _write_score_to_csv(self, writer, color, proc_score_inside, proc_score_outside, post_score_inside,
                            post_score_outside, pred_tot, post_tot):
        # for color in intensities:
        data_for_csv = (
            color, self._train_options.pre_process.name, self._train_options.post_process.name,
            self._train_options.color_space.name,
            self._train_options.classifier.name,
            self.tot_score, proc_score_inside, proc_score_outside, post_score_inside, post_score_outside, pred_tot,
            post_tot
        )
        writer.writerow(data_for_csv)

    def _test_write_score_to_csv(self, writer, color, proc_score_inside, proc_score_outside, post_score_inside,
                                 post_score_outside, pred_tot,
                                 post_tot):
        # for color in intensities:
        data_for_csv = (
            color, self._train_options.pre_process.name, self._train_options.post_process.name,
            self._train_options.color_space.name,
            self._train_options.classifier.name,
            proc_score_inside, proc_score_outside, post_score_inside, post_score_outside, pred_tot,
            post_tot
        )
        writer.writerow(data_for_csv)

    @staticmethod
    def _calc_score_inside(classes_prediction, classes_gt):
        return float(np.mean(classes_prediction[classes_gt == 1] == 1))

    @staticmethod
    def _calc_score_outside(classes_prediction, classes_gt):
        weights = np.ones_like(classes_gt)  # FIXME - need to fix weights
        return 1 - np.average(classes_prediction[classes_gt == 0] == 1, None, weights[classes_gt == 0])

    def get_options_list(self):
        return self._train_options.to_names_list()

    def _get_data_for_train(self, train_options: TrainerOptions):
        return self.data.get_data_for_train(train_options)

    def _get_data_for_test(self, train_options: TrainerOptions):
        return self.data.get_data_for_test(train_options)

    def _report_list(self):
        return self.get_options_list() + self.get_scores()

    def _debug_im_prediction(self, path, image_holder: ImageHolder,
                             classes_prediction: np.array,
                             classes_gt: np.array,
                             # classes_post: np.array = None,
                             score_in: float = 0,
                             score_out: float = 0):
        # deep_debug(f"start _debug_im_prediction for {image_holder.name}")

        data_pred_gt = self._create_debug_data(classes_prediction, classes_gt)

        debug_img = image_holder.from_array_to_im(255 * data_pred_gt, dtype=np.uint8)
        predict_image = image_holder.from_array_to_im(255 * classes_prediction, dtype=np.uint8)

        save_img(path=path,
                 img=debug_img,
                 title=image_holder.name + "_2_predict_image")

        pred_on_fg = image_holder.put_image_on_fg(image_holder.from_array_to_im(255 * classes_prediction))

        save_img(path=path,
                 img=pred_on_fg,
                 title=image_holder.name + "_3_predict_on_fg")

        # get post image
        post_start_time = time.perf_counter()
        im_post = self.post_process_image(predict_image, self._train_options.post_process)
        post_end_time = time.perf_counter()
        post_tot = post_end_time - post_start_time
        my_print(f" t0: {post_start_time} t1: {post_end_time}  post tot:{post_tot:0.4f} seconds")
        post_on_fg = image_holder.put_image_on_fg(im_post)
        save_img(path=path,
                 img=post_on_fg,
                 title=image_holder.name + "_4_post_on_fg")

        return im_post, post_tot
        # TODO - put text on images

    def _inner_predict(self, intensities):
        return self._trained_classifier.predict(intensities)

    def _save_classifier(self, path: str):
        if self._is_trained:
            path = os.path.join(path, 'finalized_model.sav')
            pickle.dump(self._trained_classifier, open(path, 'wb'))
            my_print("save classifier")

        else:
            raise BrokenPipeError("Classifier is not trained")

    @staticmethod
    def post_process_image(image, post_process):
        return post_process.get_func()(image)

    @staticmethod
    def _create_debug_data(pred_cls, gt_cls):
        data = np.zeros(pred_cls.shape + (3,))
        data[..., GT_CH] = gt_cls
        data[..., PRED_CH] = pred_cls
        data[..., MASK_CH] = gt_cls.max()
        return data


class TrainManager:
    def __init__(self, data_holder):
        self._data_holder = data_holder
        self._results = None

    # def run_on_all_options(self):
    #     for options in self._get_all_options_sets():
    #         results = self._run_on_option(options)
    #         self.add_results(options, results)

    def run_on_all_options(self):
        for clf in MlClassifier:
            train_writer, test_writer = self._open_csv_file(clf)
            for pre_process in PreprocessingActionsOptions:
                for color_space in ColorSpace:
                    for post_action in PostprocessingActionsOptions:
                        train_options = TrainerOptions(pre_process=pre_process,
                                                       classifier=clf,
                                                       post_process=post_action,
                                                       color_space=color_space
                                                       )
                        print("Classifier:", clf.name, "| Pre action:", pre_process.name, "| Color space:",
                              color_space.name, "| Post action:", post_action.name)
                        self._run_on_option(train_options, train_writer, test_writer)

    def _run_on_option(self, train_options, train_writer, test_writer):
        # TODO - handle crash with exception
        trainer = Trainer(all_data=self._data_holder, train_options=train_options)
        trainer.train(train_writer)
        trainer.test(test_writer)

    def _get_all_options_sets(self):
        pass

    def _add_results(self, results):
        self._results.append(results)
        self.write_to_file(results)

    def write_to_file(self, results):
        self._save_train_results(results)
        self._save_test_results(results)

    @staticmethod
    def _open_csv_file(classifier):
        header = ['Color name', 'Pre action', 'Post action', 'Color space', 'Classifier', 'Classifier Score',
                  'proc score In',
                  'proc score Out', 'post score In', 'post score Out', 'pred time(sec)', 'post time(sec)']

        test_header = ['Color name', 'Pre action', 'Post action', 'Color space', 'Classifier',
                       'proc score In', 'proc score Out', 'post score In', 'post score Out', 'pred time(sec)',
                       'post time(sec)']
        classifier_path = os.path.join('O:\\Shai\\projects\\mask_project\\', classifier.name)
        is_exist = os.path.exists(classifier_path)
        if not is_exist:
            os.makedirs(classifier_path)
        train_csv_file_name = classifier.name + '_train_csv_file.csv'
        train_f = open(os.path.join(classifier_path, train_csv_file_name), 'w', newline='')
        train_writer = csv.writer(train_f)
        train_writer.writerow(header)

        test_csv_file_name = classifier.name + '_test_csv_file.csv'
        test_f = open(os.path.join(classifier_path, test_csv_file_name), 'w', newline='')
        test_writer = csv.writer(test_f)
        test_writer.writerow(test_header)
        return train_writer, test_writer


def _main():
    data_holder = DataReader(BASE_PATH).read_data()
    TrainManager(data_holder).run_on_all_options()


if __name__ == '__main__':
    _main()
