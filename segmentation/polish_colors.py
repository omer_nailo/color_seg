from segmentation import Enum


class PolishColor(Enum):
    """
    Each number is the image number that each color have in image_for_cv2 folder
    """
    BL1 = 'BL1'
    BL2 = 'BL2'
    BP1 = 'BP1'
    BR1 = 'BR1'
    BR2 = 'BR2'
    G1 = 'G1'
    G3 = 'G3'
    G4 = 'G4'
    GL1 = 'GL1'
    N1 = 'N1'
    N2 = 'N2'
    N3 = 'N3'
    N4 = 'N4'
    N5 = 'N5'
    N6 = 'N6'
    O1 = 'O1'
    O2 = 'O2'
    P1 = 'P1'
    PR2 = 'PR2'
    PR3 = 'PR3'
    PR4 = 'PR4'
    PR5 = 'PR5'
    PR6 = 'PR6'
    PR7 = 'PR7'
    PR8 = 'PR8'
    PR9 = 'PR9'
    Red = 'Red'
    Y1 = 'Y1'

    def get_polish_color_number(self):
        return self.value
