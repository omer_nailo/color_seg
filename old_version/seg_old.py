import cv2, os
import numpy as np
from enum import Enum
import csv
import typing
import matplotlib.pyplot as plt
import pickle
# import classifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis as QDA
from sklearn import svm
from sklearn import tree

# from sklearn.neighbors import KNeighborsClassifier
# from sklearn.pipeline import make_pipeline
# from sklearn.preprocessing import StandardScaler
# from sklearn.gaussian_process import GaussianProcessRegressor
# from sklearn.gaussian_process.kernels import DotProduct, WhiteKernel
# from sklearn.neural_network import MLPClassifier

_DEBUG = True
# _FINGER_PREFIX = "finger_img_"
# _BRUSH_PREFIX = "bm_img_"
# _MASK_PREFIX = "prediction_img_"
# _FIT_PREFIX = "fit_img_"
_BASE_PATH = "O:\\Shai\\projects\\mask_project\\Data_img"
_PNG = ".png"
_PRE_WITH_ = "pre_with_"
_PRE_WITHOUT_ = "pre_without_"
_GT_ = "gt_"
_MASK_ = "mask_"
_LEFT = "_left"
_RIGHT = "_right"


def my_print(*args, **kwargs):
    if _DEBUG:
        print(*args, **kwargs)


class ColorSpace(Enum):
    RGB = cv2.COLOR_BGR2RGB
    YCrCb = cv2.COLOR_BGR2YCrCb
    HSV = cv2.COLOR_BGR2HSV
    HLS = cv2.COLOR_BGR2HLS
    # LAB = cv2.COLOR_BGR2LAB
    # LUV = cv2.COLOR_BGR2LUV


class PolishColor(Enum):
    """
    Each number is the image number that each color have in image_for_cv2 folder
    """
    Red = 'Red'
    PR7 = 'PR7'
    PR6 = 'PR6'
    G3 = 'G3'
    G4 = 'G4'
    N3 = 'N3'

    PR4 = 'PR4'
    O1 = 'O1'
    PR2 = 'PR2'
    N2 = 'N2'
    PR5 = 'PR5'
    N1 = 'N1'
    P1 = 'P1'
    PR3 = 'PR3'
    GL1 = 'GL1'

    def get_polish_color_number(self):
        return self.value


class PreprocessingActions:
    """
    Actions are difference between two images.
    return np.array
    """

    @staticmethod
    def subtract(im_fg: np.ndarray, im_bg: np.ndarray) -> np.ndarray:
        # cv2.absdiff: finding the absolute difference between the pixels of the two image arrays
        return cv2.absdiff(im_fg, im_bg)

    @staticmethod
    def gauss_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        # cv2.GaussianBlur: blur an image
        return PreprocessingActions.subtract(cv2.GaussianBlur(im_fg, (15, 15), 9), cv2.GaussianBlur(im_bg, (15, 15), 9))

    @staticmethod
    def median_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.medianBlur: computes the median of all the pixels under the kernel window
        and the central pixel is replaced with this median value.
        """
        return PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))

    @staticmethod
    def laplace_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.Laplacian: used to find edges in an image
        """
        fg = cv2.merge([cv2.Laplacian(ch, cv2.CV_8U) for ch in cv2.split(im_fg)])
        bg = cv2.merge([cv2.Laplacian(ch, cv2.CV_8U) for ch in cv2.split(im_bg)])
        return PreprocessingActions.subtract(fg, bg)

    @staticmethod
    def sub_and_laplace(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        return cv2.merge(
            [cv2.Laplacian(ch, cv2.CV_8U) for ch in cv2.split(PreprocessingActions.subtract(im_fg, im_bg))])

    @staticmethod
    def sobel_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.Sobel: computes an approximation of the gradient of an image intensity function
        """
        fg = cv2.merge([cv2.Sobel(ch, cv2.CV_8U, 1, 1) for ch in cv2.split(im_fg)])
        bg = cv2.merge([cv2.Sobel(ch, cv2.CV_8U, 1, 1) for ch in cv2.split(im_bg)])
        return PreprocessingActions.subtract(fg, bg)

    @staticmethod
    def scharr_and_sub(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.Scharr: Scharr kernel may give us better approximations to the gradient then Sobel kernel
        """
        fg = cv2.merge([cv2.Scharr(ch, cv2.CV_8U, 1, 0) for ch in cv2.split(im_fg)])
        bg = cv2.merge([cv2.Scharr(ch, cv2.CV_8U, 1, 0) for ch in cv2.split(im_bg)])
        return PreprocessingActions.subtract(fg, bg)

    @staticmethod
    def hist_eq_cutoff(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        res = PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))
        return np.uint8(255 * (cv2.merge([cv2.equalizeHist(ch) for ch in cv2.split(res)]) > 240))

    @staticmethod
    def hist_eq(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.equalizeHist: contrast adjustment using the image's histogram.
        useful in images with backgrounds and foregrounds that are both bright or both dark
        """
        res = PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))
        return np.uint8(255 * (cv2.merge([cv2.equalizeHist(ch) for ch in cv2.split(res)])))

    @staticmethod
    def hist_eq_median(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        res = PreprocessingActions.subtract(cv2.medianBlur(im_fg, 9), cv2.medianBlur(im_bg, 9))
        res = np.uint8(255 * (cv2.merge([cv2.equalizeHist(ch) for ch in cv2.split(res)])))
        return cv2.medianBlur(res, 5)

    @staticmethod
    def subtract_and_normalize(im_fg: np.array, im_bg: np.array) -> np.ndarray:
        """
        cv2.normalize: changing the pixel intensity and increasing the overall contrast
        """
        diff_img = PreprocessingActions.subtract(im_fg, im_bg)
        norm_img = np.zeros_like(diff_img)
        return cv2.normalize(diff_img, norm_img, 0, 255, cv2.NORM_MINMAX)


class PreprocessingActionsOptions(Enum):
    """
    Value is the function from Actions
    """
    # SUBTRACT = (PreprocessingActions.subtract,)
    # HIST_EQ = (PreprocessingActions.hist_eq,)
    # SUBTRACT_AND_NORMALIZE = (PreprocessingActions.subtract_and_normalize,)
    # GAUSS_AND_SUB = (PreprocessingActions.gauss_and_sub,)
    # MEDIAN_AND_SUB = (PreprocessingActions.median_and_sub,)
    # LAPLACE_AND_SUB = (PreprocessingActions.laplace_and_sub,)
    # SUB_AND_LAPLACE = (PreprocessingActions.sub_and_laplace,)
    # SOBEL_AND_SUB = (PreprocessingActions.sobel_and_sub,)
    # SCHARR_AND_SUB = (PreprocessingActions.scharr_and_sub,)
    # HIST_EQ_CUTOFF = (PreprocessingActions.hist_eq_cutoff,)
    HIST_EQ_MEDIAN = (PreprocessingActions.hist_eq_median,)

    def get_func(self):
        return self.value[0]


class PostprocessingActions:
    @staticmethod
    def morph(image: np.array) -> np.array:
        im_open = cv2.morphologyEx(image, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15)))
        im_close = cv2.morphologyEx(im_open, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
        return PostprocessingActions._make_convex(im_close)

    @staticmethod
    def median(image: np.array) -> np.array:
        im_median = cv2.medianBlur(image, 15)
        return PostprocessingActions._make_convex(im_median)

    @staticmethod
    def close_median(image, kernel_size=7):
        im_close = cv2.morphologyEx(image, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
        im_median = cv2.medianBlur(im_close, kernel_size)
        return PostprocessingActions._make_convex(im_median)

    @staticmethod
    def close_convex(image: np.array):
        im_close = cv2.morphologyEx(image, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15)))
        return PostprocessingActions._make_convex(im_close)

    @staticmethod
    def close_open_median_convex(image: np.array):
        im_median = cv2.medianBlur(image, 9)
        im_close = cv2.morphologyEx(im_median, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
        im_open = cv2.morphologyEx(im_close, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))

        return PostprocessingActions._make_convex(im_open)

    @staticmethod
    def nothing(image: np.array):
        return np.uint8(255 * image)

    @staticmethod
    def _make_convex(image: np.array, area_thresh: int = 100) -> np.array:
        """

        Args:
            image: binary image for convex hull
            area_thresh:

        Returns:

        """
        # contours = cv2.findContours(np.array(image).astype(np.uint8),
        #                             cv2.RETR_EXTERNAL,
        #                             cv2.CHAIN_APPROX_SIMPLE)[-2]
        contours = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

        # convex_contour = [cv2.convexHull(cnt, False) for cnt in contours if cv2.contourArea(cnt) > area_thresh]

        contours_filters = [cnt for cnt in contours if cv2.contourArea(cnt) > area_thresh]
        convex_image = cv2.drawContours(np.zeros_like(image, dtype=np.uint8), contours_filters, -1, 255, -1)
        from skimage.morphology import convex_hull_image
        return np.uint8(255 * convex_hull_image(convex_image))
        # return cv2.drawContours(np.zeros_like(image, dtype=np.uint8), convex_contour, -1, 255, -1)


class PostprocessingActionsOptions(Enum):
    """
    Value is the function from PostprocessingActions
    """
    MORPH = (PostprocessingActions.morph,)
    MEDIAN = (PostprocessingActions.median,)
    CLOSE_MEDIAN = (PostprocessingActions.close_median,)

    CLOSE_CONVEX = (PostprocessingActions.close_convex,)

    CLOSE_OPEN_MEDIAN_CONVEX = (PostprocessingActions.close_open_median_convex,)
    NOTHING = (PostprocessingActions.nothing,)

    def get_func(self):
        return self.value[0]


class MlClassifier(Enum):
    """
    Classifier Objects
    """
    # LDA = LDA()
    # QDA = QDA()
    # KNN= KNeighborsClassifier(n_neighbors=5)
    DecisionTree = tree.DecisionTreeClassifier()
    # SVM = svm.SVC()
    # GPR = GaussianProcessRegressor(kernel=DotProduct()+WhiteKernel(),random_state = 0)

    # for neural net:
    # https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
    # NEURAL_NET = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes = (5, 2), random_state = 1)


class TestPolishColors(Enum):
    BP1 = 'BP1'
    BL1 = 'BL1'
    PR8 = 'PR8'
    Y1 = 'Y1'
    G1 = 'G1'

    PR9 = 'PR9'
    BR1 = 'BR1'
    BR2 = 'BR2'
    BL2 = 'BL2'

    N4 = 'N4'
    N5 = 'N5'
    N6 = 'N6'
    O2 = 'O2'
    # from train
    Red = 'Red'
    PR7 = 'PR7'
    N2 = 'N2'

    def get_test_color_value(self):
        return self.value


# ------------------------------------------- TRAIN -------------------------------------------

class Test:
    """
    For actions on set of classifier + color space + action + polish color
    """

    def __init__(self, pre_action, post_action, color_space, classifier):
        self._pre_action = pre_action.get_func()
        self._color_space = color_space
        self._classifier = classifier
        self._post_action = post_action.get_func()

    @property
    def pre_action(self):
        return self._pre_action

    @property
    def color_space(self):
        return self._color_space

    @property
    def classifier(self):
        return self._classifier

    @property
    def post_action(self):
        return self._post_action

    @staticmethod
    def calc_weights(image_holder) -> np.ndarray:
        rc_com = np.mean(np.argwhere(image_holder.gt_img > 0), axis=0)
        data_pos = np.argwhere(image_holder.nail_mask_img > 0)
        dist = np.linalg.norm(data_pos - rc_com, axis=1)
        weights = 1 / dist ** 2
        weights = np.ones_like(weights)
        return weights


class ImageHolder:

    def __init__(self, polish_color, fg_img, bg_img, nail_mask_img, gt_mask_img, color_space):
        self._color_name = polish_color
        self._bg_img = bg_img
        self._fg_img = fg_img
        self._gt_mask_img = gt_mask_img
        self._nail_mask_img = nail_mask_img
        self._pre_processed_img = None
        self._color_space = color_space

    @property
    def bg_img(self):
        return self._bg_img.copy()

    @property
    def fg_img(self):
        return self._fg_img.copy()

    @property
    def gt_img(self):
        return self._gt_mask_img.copy()

    @property
    def nail_mask_img(self):
        return self._nail_mask_img.copy()

    @property
    def color_name(self):
        return self._color_name

    @property
    def color_space(self):
        return self._color_space

    @property
    def processed_img(self):
        return self._pre_processed_img

    @processed_img.setter
    def processed_img(self, image: np.array):
        self._pre_processed_img = image.copy()

    def calc_and_set_processed_image(self, action):
        bg = cv2.cvtColor(self._bg_img, self._color_space.value)
        fg = cv2.cvtColor(self._fg_img, self._color_space.value)

        fg[self._nail_mask_img == 0] = 0
        bg[self._nail_mask_img == 0] = 0

        self._pre_processed_img = action(fg, bg)
        # TODO: POST ACTION image


class ImageHandler:

    @staticmethod
    def _get_image(polish_color: PolishColor) -> tuple:
        my_print("  start get image")
        bg_path = os.path.join(_BASE_PATH, _PRE_WITHOUT_ + polish_color.name + _PNG)  # bg -> finger
        bg_img = cv2.imread(bg_path)

        fg_path = os.path.join(_BASE_PATH, _PRE_WITH_ + polish_color.name + _PNG)  # fg -> bm
        fg_img = cv2.imread(fg_path)

        nail_mask_path = os.path.join(_BASE_PATH, _MASK_ + polish_color.name + _PNG)
        nail_mask_img = cv2.imread(nail_mask_path, 0)

        fg_mask_path = os.path.join(_BASE_PATH, _GT_ + polish_color.name + _PNG)
        fg_mask_img = cv2.imread(fg_mask_path, 0)

        return fg_img, bg_img, nail_mask_img, fg_mask_img

    @staticmethod
    def prepare_data(polish_color: PolishColor, color_space: ColorSpace, pre_action: PreprocessingActionsOptions) -> [
        ImageHolder,
        np.ndarray]:
        my_print("  start prepare data")
        fg_img, bg_img, nail_mask_img, gt_mask_img = ImageHandler._get_image(polish_color)
        image_holder = ImageHolder(polish_color, fg_img, bg_img, nail_mask_img, gt_mask_img,
                                   color_space)
        my_print(f"start pre action: {pre_action.__name__}")
        image_holder.calc_and_set_processed_image(pre_action)

        return image_holder


class DataHandler:
    """
    Make action on data
    """

    def __init__(self, test):
        self.test = test
        # prepare data by color
        self.intensity_and_target_class_to_fit = self.get_data_to_fit_dictionary()
        # trained classifier object
        self.ml_classifier_trained = self.fit_with_classifier()

    def get_data_to_fit_dictionary(self) -> dict:
        intensity_and_target_class_to_fit = dict()

        for polish_color in PolishColor:
            my_print(
                f"get_data_to_fit_dictionary with pre: {self.test.pre_action} | colorspace: {self.test.color_space} | classifier: {self.test.classifier}")
            image_holder = ImageHandler.prepare_data(polish_color=polish_color,
                                                     color_space=self.test.color_space,
                                                     pre_action=self.test._pre_action)

            intensity_and_target_class_to_fit[polish_color] = self._get_data_for_fit(image_holder=image_holder)

        return intensity_and_target_class_to_fit

    def _get_data_for_fit(self, image_holder: ImageHolder) -> np.ndarray:
        my_print(
            f"start get data for fit with pre: {self.test.pre_action} | colorspace: {self.test.color_space} | classifier: {self.test.classifier}")
        nail_mask_img = image_holder.nail_mask_img
        gt_mask_img = image_holder._gt_mask_img
        pre_processed_img = image_holder._pre_processed_img
        gt_y_fit = gt_mask_img[nail_mask_img > 0]
        proc_x_fit = pre_processed_img[nail_mask_img > 0]

        gt_y_fit[gt_y_fit > 0] = 1
        return np.int32(np.hstack([proc_x_fit, gt_y_fit[:, np.newaxis]]))

    def fit_with_classifier(self) -> MlClassifier:
        my_print("start fit with classifier-")
        # make data type np.array from dict
        classifier_as_np = np.vstack(list(self.intensity_and_target_class_to_fit.values()))
        classifier_training_vectors = classifier_as_np[:, :3]
        # my_print("after classifier_training_vectors")
        classifier_target_values = classifier_as_np[:, 3]
        my_print(f"start fit")
        self.test.classifier.value.fit(classifier_training_vectors, classifier_target_values)
        my_print("after fit")

        return self.test.classifier

    def analyze_with_classifier(self) -> tuple:
        """
        Returns: Tuple(float, list)
            round(score_tot, 3) -> round score out
            score_per_color_list -> list of tuples (color name, (score in, score out))
        """
        my_print(f"start analyze")

        score_per_color_list = self.get_score_per_color_list()
        my_print("analyze 1 ")
        score_tot = self._evaluate_classifier_fit()
        my_print("analyze 2 ")
        return round(score_tot, 3), score_per_color_list

    def get_score_per_color_list(self) -> list:
        score_per_color_list = list()
        my_print("get_score_per_color_list")
        for polish_color in PolishColor:
            my_print(f"get_score_per_color_list for {polish_color}")
            # Calc weights by distance from brush mask center of mass
            image_holder = ImageHandler.prepare_data(polish_color=polish_color,
                                                     color_space=self.test.color_space,
                                                     pre_action=self.test._pre_action,
                                                     )
            weights = self.test.calc_weights(image_holder)
            # Create predict image [clf.predict(pre_processed)] -> binary image
            path_to_save_trained_classifier, predict_results, post_pred_results = self._predict_on_image_classifier(
                polish_color,
                image_holder)

            # Calc score inside and outside
            data = self.intensity_and_target_class_to_fit[polish_color]
            classes_prediction_after_predict = self.ml_classifier_trained.value.predict(data[:, :3])
            classes_gt = data[:, 3]
            score_per_color_after_predict = self._calc_score(classes_prediction_after_predict, classes_gt, weights)

            classes_prediction_after_post = post_pred_results
            score_per_color_after_post = self._calc_score(classes_prediction_after_post, classes_gt, weights)

            score_per_color_list.append((polish_color.name, score_per_color_after_predict, score_per_color_after_post))
        # Save trained classifier
        self._save_classifier(self.ml_classifier_trained, path_to_save_trained_classifier)
        # Return score per color for pre processed

        return score_per_color_list

    def _calc_score(self, classes_prediction, classes_gt, weights: np.array):
        # Score Inside
        score_inside = float(np.mean(classes_prediction[classes_gt == 1] == 1))

        # Score Outside
        score_outside = 1 - np.average(classes_prediction[classes_gt == 0] == 1, None, weights[classes_gt == 0])
        return round(score_inside, 3), round(score_outside, 3)

    def _predict_on_image_classifier(self, polish_color, image_holder: ImageHolder):
        classifier_path = os.path.join("O:\\Shai\\projects\\mask_project", self.test.classifier.name, 'Train',
                                       self.test.pre_action.__name__ + '_' + self.test.post_action.__name__,
                                       self.test.color_space.name)
        my_print("  start predict on image")
        # get the image (processed) + nail prediction
        # fg_img = cv2.cvtColor(image_holder.fg_img, cv2.COLOR_BGR2RGB)
        fg_img = image_holder.fg_img
        pre_processed_img = image_holder.processed_img

        # set an empty image
        predict_image = np.zeros_like(fg_img)
        fit_where_nail_mask = pre_processed_img[image_holder.nail_mask_img > 0]
        predict_results = self.ml_classifier_trained.value.predict(fit_where_nail_mask)

        # set values in class 1 as 255 in the empty image
        predict_image[image_holder.nail_mask_img > 0, :2] = predict_results[:, np.newaxis]
        predict_image[predict_image > 0] = 255
        self._save_image(classifier_path, polish_color, predict_image, 'predict_image')

        # blend with original image
        # ------- pre_processed_img = cv2.cvtColor(pre_processed_img[..., 0], cv2.COLOR_GRAY2BGR)-------
        self._save_image(classifier_path, polish_color, pre_processed_img, 'pre_processed_img')
        my_print(f"start post action with post:{self.test.post_action}")
        im_post = cv2.cvtColor(self.test.post_action(predict_image[..., 0]), cv2.COLOR_GRAY2BGR)
        predict_on_post = cv2.addWeighted(fg_img, 0.7, im_post, 0.3, 0)

        post_pred_results = im_post[image_holder.nail_mask_img > 0, 0]
        post_pred_results[post_pred_results > 0] = 1
        # post_pred_results = self.ml_classifier_trained.value.predict(after_post_fit_where_nail_mask)

        self._save_image(classifier_path, polish_color, predict_on_post, 'predict_on_post_img')

        # _ = self._save_image(polish_color, pre_processed_img, 'pre_processed_img')
        # path is to save in same path the classifier
        return classifier_path, predict_results, post_pred_results

    def _save_image(self, classifier_path, polish_color, img: np.ndarray, name):
        """
        saves images after action to folder
        """
        my_print("  start save image-", polish_color)
        is_exist = os.path.exists(classifier_path)
        if not is_exist:
            os.makedirs(classifier_path)
        img_name = "debug_" + str(polish_color) + "_" + name + _PNG
        cv2.imwrite(os.path.join(classifier_path, img_name), cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        my_print(f"    image {img_name} saved")

    def _save_classifier(self, trained_classifier: MlClassifier, path: str):
        path = os.path.join(path, 'finalized_model.sav')
        pickle.dump(trained_classifier.value, open(path, 'wb'))

    def _evaluate_classifier_fit(self) -> float:
        my_print("start evaluate")
        unify_classifier = np.vstack(list(self.intensity_and_target_class_to_fit.values()))
        # zero is bad, 1 is good
        my_print("evaluate 1")
        score = self.ml_classifier_trained.value.score(unify_classifier[:, :3], unify_classifier[:, 3])
        my_print("evaluate - score")
        return score


# ------------------------------------------- TEST -------------------------------------------

class TestClassifierPrediction:

    def __init__(self, pre_action, post_action, color_space, color, side, classifier, clf_name):
        self.pre_action = pre_action
        self.color = color
        self.side = side
        self.color_space = color_space
        self.post_action = post_action
        self.classifier = classifier
        self.clf_name = clf_name

    # def test_classifier_prediction(self, clf, fg_img, bg_img, nail_mask_img):  # finger, bm
    #     # clf = self._load_clf()
    #     fg_test = cv2.cvtColor(fg_img.copy(), self.color_space.value)
    #     bg_test = cv2.cvtColor(bg_img.copy(), self.color_space.value)
    #     fg_test[nail_mask_img == 0] = 0
    #     bg_test[nail_mask_img == 0] = 0
    #     pre_processed = self.pre_action(fg_test, bg_test)
    #     post_image, post_on_fg_debug, clf_res_img = self.processed(clf, pre_processed, fg_img, nail_mask_img)
    #
    # # self._show_images_on_axes(fg_img, after_pre_processed, clf_res_img, post_image, post_on_fg_debug)
    #
    # def _show_images_on_axes(self, fg_img, after_pre_processed, clf_res_img, post_image, post_on_fg_debug):
    #     fig, axes = plt.subplots(2, 3)
    #
    #     axes[0, 0].imshow(fg_img[..., ::-1])
    #     axes[0, 0].set_title(self.color + self.side)
    #
    #     axes[0, 1].imshow(after_pre_processed)
    #     axes[0, 1].set_title('after_pre_processed ' + self.color + self.side + ' : ' + self.pre_action.__name__)
    #
    #     axes[0, 2].imshow(clf_res_img)
    #     axes[0, 2].set_title(
    #         'processed image (before post) ' + self.color + self.side + ' : ' + self.pre_action.__name__)
    #
    #     axes[1, 0].imshow(post_image)
    #     axes[1, 0].set_title('post_image ' + self.color + self.side + ' : ' + self.pre_action.__name__)
    #
    #     axes[1, 1].imshow(post_on_fg_debug)
    #     axes[1, 1].set_title('post_on_fg_debug  ' + self.color)
    #     plt.show()
    #
    # def _load_clf(self):
    #     path = os.path.join('O:\\Shai\\projects\\mask_project', str(self.classifier), 'Train',
    #                         self.pre_action.__name__ + '_' + self.post_action.__name__,
    #                         self.color_space.name, 'finalized_model.sav')
    #     loaded_model = pickle.load(open(path, 'rb'))
    #     return loaded_model
    #
    # def processed(self, clf, pre_processed, fg_img, nail_mask_img) -> np.array:
    #     # bg_img = cv2.cvtColor(bg_img, cv2.COLOR_BGR2RGB)
    #     processed_img = pre_processed
    #     # set an empty image
    #     clf_res_img = np.zeros_like(processed_img)
    #     fit_where_nail_mask = processed_img[nail_mask_img > 0]
    #     predict_results = clf.predict(fit_where_nail_mask)
    #     # set values in class 1 as 255 in the empty image
    #     clf_res_img[nail_mask_img > 0, :2] = predict_results[:, np.newaxis]
    #     clf_res_img[clf_res_img > 0] = 255
    #
    #     # blend with original image
    #     post_image = cv2.cvtColor(self.post_action(clf_res_img[..., 0]), cv2.COLOR_GRAY2BGR)
    #     post_on_fg_debug = cv2.addWeighted(post_image, 0.3, fg_img, 0.7, 0)
    #
    #     self._save_test_image(processed_img, 'pre_processed_img')
    #     self._save_test_image(clf_res_img, 'predict_image')
    #     self._save_test_image(post_on_fg_debug, 'predict_on_post_img')
    #
    #     return post_image, post_on_fg_debug, clf_res_img
    #
    # def _save_test_image(self, image_debug, name):
    #     my_print("  start save test image-")
    #     base_path = os.path.join(
    #         'O:\\Shai\\projects\\mask_project\\' + self.clf_name + '\\Tests\\' + str(
    #             self.pre_action.__name__) + "_" + self.post_action.__name__, self.color_space.name)
    #     is_exist = os.path.exists(base_path)
    #     if not is_exist:
    #         os.makedirs(base_path)
    #     img_name = "Test_" + self.color + self.side + "_" + name + _PNG
    #     cv2.imwrite(os.path.join(base_path, img_name), cv2.cvtColor(image_debug, cv2.COLOR_BGR2RGB))
    #     my_print(f"    image {img_name} saved")


class TestImageHolder:

    def __init__(self, polish_color, fg_img, bg_img, gt_img, nail_mask_img, color_space, clf):
        self._color_name = polish_color
        self._bg_img = bg_img
        self._fg_img = fg_img
        self._gt_img = gt_img
        self._nail_mask_img = nail_mask_img
        self._pre_processed_img = None
        self._color_space = color_space
        self.classifier_name = clf

    @property
    def bg_img(self):
        return self._bg_img.copy()

    @property
    def fg_img(self):
        return self._fg_img.copy()

    @property
    def gt_img(self):
        return self._gt_img.copy()

    @property
    def nail_mask_img(self):
        return self._nail_mask_img.copy()

    @property
    def color_name(self):
        return self._color_name

    @property
    def color_space(self):
        return self._color_space

    @property
    def processed_img(self):
        return self._pre_processed_img

    @processed_img.setter
    def processed_img(self, image: np.array):
        self._pre_processed_img = image.copy()

    def calc_and_set_processed_image(self, action):
        bg = cv2.cvtColor(self._bg_img, self._color_space.value)
        fg = cv2.cvtColor(self._fg_img, self._color_space.value)

        fg[self._nail_mask_img == 0] = 0
        bg[self._nail_mask_img == 0] = 0

        self._pre_processed_img = action(fg, bg)
        # TODO: POST ACTION image


class TestImageHandler:
    # TODO: ask Omer if better to forward int or polish
    """def _get_image(polish_color: PolishColor) -> tuple:
        color_number= polish_color.get_polish_color_number()"""

    @staticmethod
    def _get_image(color, side):
        my_print("  start get image")

        fg_path = os.path.join(_BASE_PATH, _PRE_WITH_ + color.value + side + _PNG)
        try:
            fg_img = cv2.imread(fg_path)
            bg_img = cv2.imread(os.path.join(_BASE_PATH, _PRE_WITHOUT_ + color.value + side + _PNG))
            gt_img = cv2.imread(os.path.join(_BASE_PATH, _GT_ + color.value + side + _PNG), 0)
            nail_mask_img = cv2.imread(os.path.join(_BASE_PATH, _MASK_ + color.value + side + _PNG), 0)

        except AttributeError as e:
            print(e)
            return None, None, None, None

        return fg_img, bg_img, gt_img, nail_mask_img

    @staticmethod
    def prepare_data(polish_color: PolishColor, color_space: ColorSpace, pre_action: PreprocessingActionsOptions,
                     post_action, side, trained_classifier, classifier_name) -> [
        TestImageHolder,
        np.ndarray]:
        my_print("  start prepare data")
        # try
        # except - no left image (return none)

        fg_img, bg_img, gt_img, nail_mask_img = TestImageHandler._get_image(polish_color, side)
        ##############################################################################################################################
        # gt None for colors from train (no sides)
        ##############################################################################################################################
        if gt_img is None: print(f"gt_img is None for {polish_color}")
        if gt_img is not None:
            image_holder = TestImageHolder(polish_color, fg_img, bg_img, gt_img, nail_mask_img,
                                           color_space, classifier_name)

            # TestClassifierPrediction(pre_action, post_action, color_space, polish_color.value,
            #                          side, trained_classifier, image_holder.classifier_name).test_classifier_prediction(trained_classifier, fg_img,
            #                                                                               bg_img, nail_mask_img)

            my_print(f"start pre action: {pre_action.__name__}")
            image_holder.calc_and_set_processed_image(pre_action)
            return image_holder

        return None


class TestDataHandler:
    """
    Make action on data
    """

    def __init__(self, test, side, ml_classifier_trained, clf):
        self.test = test
        self.side = side
        self.ml_classifier = clf
        # trained classifier object
        self.ml_classifier_trained = ml_classifier_trained
        # prepare data by color
        self.intensity_and_target_class_to_fit = self.get_data_to_fit_dictionary()

    def get_data_to_fit_dictionary(self) -> dict:
        intensity_and_target_class_to_fit = dict()

        for polish_color in TestPolishColors:
            my_print(
                f"get_data_to_fit_dictionary with pre: {self.test.pre_action} | colorspace: {self.test.color_space} | classifier: {self.test.classifier}")
            image_holder = TestImageHandler.prepare_data(polish_color=polish_color,
                                                         color_space=self.test.color_space,
                                                         pre_action=self.test.pre_action,
                                                         post_action=self.test.post_action,
                                                         side=self.side,
                                                         trained_classifier=self.ml_classifier_trained,
                                                         classifier_name=self.test.classifier.name)

            if image_holder is not None:
                intensity_and_target_class_to_fit[polish_color.name + self.side] = self._get_data_for_fit(
                    image_holder=image_holder)

        return intensity_and_target_class_to_fit

    def _get_data_for_fit(self, image_holder: TestImageHolder) -> np.ndarray:
        my_print(
            f"start get data for fit with pre: {self.test.pre_action} | colorspace: {self.test.color_space} | classifier: {self.test.classifier}")
        nail_mask_img = image_holder.nail_mask_img
        gt_mask_img = image_holder.gt_img
        pre_processed_img = image_holder.processed_img
        gt_y_fit = gt_mask_img[nail_mask_img > 0]
        proc_x_fit = pre_processed_img[nail_mask_img > 0]

        gt_y_fit[gt_y_fit > 0] = 1
        return np.column_stack([proc_x_fit, gt_y_fit[:, np.newaxis]])

    def analyze_with_classifier(self) -> tuple:
        """
        Returns: Tuple(float, list)
            round(score_tot, 3) -> round score out
            score_per_color_list -> list of tuples (color name, (score in, score out))
        """
        my_print(f"start analyze")

        score_per_color_list = self.get_score_per_color_list()
        my_print("analyze 1 ")
        score_tot = self._evaluate_classifier_fit()
        my_print("analyze 2 ")
        return round(score_tot, 3), score_per_color_list

    def get_score_per_color_list(self) -> list:
        score_per_color_list = list()
        my_print("get_score_per_color_list")
        for polish_color in TestPolishColors:
            my_print(f"get_score_per_color_list for {polish_color}")
            # Calc weights by distance from brush mask center of mass
            image_holder = TestImageHandler.prepare_data(polish_color=polish_color,
                                                         color_space=self.test.color_space,
                                                         pre_action=self.test.pre_action,
                                                         post_action=self.test.post_action,
                                                         side=self.side,
                                                         trained_classifier=self.ml_classifier_trained,
                                                         classifier_name=self.test.classifier.name)
            if image_holder is None:
                continue

            weights = self.test.calc_weights(image_holder)
            # Create predict image [clf.predict(pre_processed)] -> binary image
            predict_results, post_pred_results = self._predict_on_image_classifier(
                polish_color,
                image_holder)

            # Calc score inside and outside
            data = self.intensity_and_target_class_to_fit[polish_color.name + self.side]
            classes_prediction_after_predict = self.ml_classifier_trained.predict(data[:, :3])
            classes_gt = data[:, 3]
            score_per_color_after_predict = self._calc_score(classes_prediction_after_predict, classes_gt, weights)

            classes_prediction_after_post = post_pred_results
            score_per_color_after_post = self._calc_score(classes_prediction_after_post, classes_gt, weights)

            score_per_color_list.append(
                (polish_color.name + self.side, score_per_color_after_predict, score_per_color_after_post))

        return score_per_color_list

    def _predict_on_image_classifier(self, polish_color, image_holder: ImageHolder):
        dir_path = os.path.join("O:\\Shai\\projects\\mask_project", self.test.classifier.name, 'Test',
                                self.test.pre_action.__name__ + '_' + self.test.post_action.__name__,
                                self.test.color_space.name)
        my_print("  start predict on image")
        # get the image (processed) + nail prediction
        # fg_img = cv2.cvtColor(image_holder.fg_img, cv2.COLOR_BGR2RGB)
        fg_img = image_holder.fg_img
        pre_processed_img = image_holder.processed_img

        # set an empty image
        predict_image = np.zeros_like(fg_img)
        fit_where_nail_mask = pre_processed_img[image_holder.nail_mask_img > 0]
        predict_results = self.ml_classifier_trained.predict(fit_where_nail_mask)

        # set values in class 1 as 255 in the empty image
        predict_image[image_holder.nail_mask_img > 0, :2] = predict_results[:, np.newaxis]
        predict_image[predict_image > 0] = 255
        self._save_image(dir_path, polish_color, predict_image, 'predict_image')

        # blend with original image
        # ------- pre_processed_img = cv2.cvtColor(pre_processed_img[..., 0], cv2.COLOR_GRAY2BGR)-------
        self._save_image(dir_path, polish_color, pre_processed_img, 'pre_processed_img')
        my_print(f"start post action with post:{self.test.post_action}")
        im_post = cv2.cvtColor(self.test.post_action(predict_image[..., 0]), cv2.COLOR_GRAY2BGR)
        predict_on_post = cv2.addWeighted(fg_img, 0.7, im_post, 0.3, 0)

        post_pred_results = im_post[image_holder.nail_mask_img > 0, 0]
        post_pred_results[post_pred_results > 0] = 1
        # post_pred_results = self.ml_classifier_trained.value.predict(after_post_fit_where_nail_mask)

        self._save_image(dir_path, polish_color, predict_on_post, 'predict_on_post_img')

        # _ = self._save_image(polish_color, pre_processed_img, 'pre_processed_img')
        # path is to save in same path the classifier
        return predict_results, post_pred_results

    def _save_image(self, classifier_path, polish_color, img: np.ndarray, name):
        """
        saves images after action to folder
        """
        my_print("  start save image-", polish_color)
        is_exist = os.path.exists(classifier_path)
        if not is_exist:
            os.makedirs(classifier_path)
        img_name = "debug_" + str(polish_color) + self.side + "_" + name + _PNG
        cv2.imwrite(os.path.join(classifier_path, img_name), cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        my_print(f"    image {img_name} saved")

    @staticmethod
    def _calc_score(classes_prediction, classes_gt, weights: np.array):
        # Score Inside
        score_inside = float(np.mean(classes_prediction[classes_gt == 1] == 1))

        # Score Outside
        score_outside = 1 - np.average(classes_prediction[classes_gt == 0] == 1, None, weights[classes_gt == 0])
        return round(score_inside, 3), round(score_outside, 3)

    def _evaluate_classifier_fit(self) -> float:
        my_print("start evaluate")
        unify_classifier = np.vstack(list(self.intensity_and_target_class_to_fit.values()))
        # zero is bad, 1 is good
        my_print("evaluate 1")
        score = self.ml_classifier_trained.score(unify_classifier[:, :3], unify_classifier[:, 3])
        my_print("evaluate - score")
        return score


def load_and_predict(color: TestPolishColors, pre_action: PreprocessingActionsOptions,
                     post_action: PostprocessingActions,
                     color_space: ColorSpace, classifier):
    base_path = os.path.join('O:\\Shai\\projects\\mask_project\\Data_img')
    for side in [_LEFT, _RIGHT]:
        fg_path = os.path.join(base_path, _PRE_WITH_ + color.value + side + _PNG)
        try:
            fg_img = cv2.imread(fg_path)
            bg_img = cv2.imread(os.path.join(base_path, _PRE_WITHOUT_ + color.value + side + _PNG))
            nail_mask_img = cv2.imread(os.path.join(base_path, _PRE_PREDICTION_ + color.value + side + _PNG), 0)
            TestClassifierPrediction(pre_action.get_func(), post_action, color_space, color.value,
                                     side, classifier).test_classifier_prediction(fg_img, bg_img, nail_mask_img)

        except AttributeError as e:
            print(e)
            continue
    # return test_analyze_score, test_score_per_color_list


# ------------------------------------------- RUN  -------------------------------------------
def open_csv_file(classifier):
    header = ['Color name', 'Pre action', 'Post action', 'Color space', 'Classifier', 'Classifier Score',
              'proc score In',
              'proc score Out', 'post score In', 'post score Out']
    classifier_path = os.path.join('O:\\Shai\\projects\\mask_project\\', classifier.name)
    is_exist = os.path.exists(classifier_path)
    if not is_exist:
        os.makedirs(classifier_path)
    train_csv_file_name = classifier.name + '_train_csv_file.csv'
    train_f = open(os.path.join(classifier_path, train_csv_file_name), 'w', newline='')
    train_writer = csv.writer(train_f)
    train_writer.writerow(header)

    test_csv_file_name = classifier.name + '_test_csv_file.csv'
    test_f = open(os.path.join(classifier_path, test_csv_file_name), 'w', newline='')
    test_writer = csv.writer(test_f)
    test_writer.writerow(header)
    return train_writer, test_writer


def train_and_test_per_tests():
    for classifier in MlClassifier:
        # Open CSV file
        train_writer, test_writer = open_csv_file(classifier)
        for pre_action in PreprocessingActionsOptions:
            for color_space in ColorSpace:
                for post_action in PostprocessingActionsOptions:
                    print("Classifier:", classifier.name, "| Pre action:", pre_action.name, "| Color space:",
                          color_space.name,
                          "| Post action:", post_action.name)
                    # TRAIN
                    test = Test(pre_action, post_action, color_space, classifier)
                    analyze_score, score_per_color_list = DataHandler(test).analyze_with_classifier()

                    # TEST
                    test_score_per_color_list = list()
                    test_analyze_score = None
                    # for color in TestPolishColors:
                    # load_and_predict(color, pre_action,
                    #                  post_action.get_func(),
                    #                  color_space, classifier)
                    path = os.path.join('O:\\Shai\\projects\\mask_project', classifier.name, 'Train',
                                        pre_action.name + '_' + post_action.name,
                                        color_space.name, 'finalized_model.sav')
                    loaded_model = pickle.load(open(path, 'rb'))
                    for side in [_LEFT, _RIGHT]:
                        test_data_handler = TestDataHandler(test, side, loaded_model, classifier)
                        if test_data_handler is not None:
                            test_analyze_score, test_score_per_color_list = test_data_handler.analyze_with_classifier()

                        for i in range(len(test_score_per_color_list)):
                            test_data_for_csv = (test_score_per_color_list[i][0], pre_action.name, post_action.name,
                                                 color_space.name, classifier.name,
                                                 test_analyze_score,
                                                 test_score_per_color_list[i][1][0],
                                                 test_score_per_color_list[i][1][1],
                                                 test_score_per_color_list[i][2][0],
                                                 test_score_per_color_list[i][2][1],

                                                 )
                            test_writer.writerow(test_data_for_csv)

                    for i in range(len(score_per_color_list)):
                        data_for_csv = (
                            score_per_color_list[i][0], pre_action.name, post_action.name, color_space.name,
                            classifier.name,
                            analyze_score,
                            score_per_color_list[i][1][0],
                            score_per_color_list[i][1][1],
                            score_per_color_list[i][2][0],
                            score_per_color_list[i][2][1],
                        )
                        # write line to csv
                        train_writer.writerow(data_for_csv)


if __name__ == "__main__":
    train_and_test_per_tests()

"""
if __name__ == "__main__":
    # pre_action = PreprocessingActions.hist_eq_median
    # color_space = ColorSpace.RGB
    # post_action = PostprocessingActionsOptions.MORPH
    # classifier = 'DecisionTree'
    # for color in TestPolishColors:
    #     load_and_predict(color.value, pre_action, post_action.get_func(), color_space, classifier)

    # train_data_and_save_clfs(post_action)

def train_data_and_save_clfs(post_action):
    # analyze_score, score_per_color_list = None, None
    # for csv file
    header = ['Color name', 'Action', 'Color space', 'Classifier', 'Classifier Score', 'Score In', 'Score Out']
    f = open('O:\\Shai\\projects\\mask_project\\DTREE_csv_file.csv', 'w', newline='')
    writer = csv.writer(f)
    writer.writerow(header)

    for classifier in MlClassifier:
        for pre_action in PreprocessingActionsOptions:
            for color_space in ColorSpace:
                # TRAIN
                print("Classifier:", classifier.name, "| Action:", pre_action.name, "| Color space:", color_space.name,
                      "| Post action:", post_action)
                test = Test(pre_action, post_action, color_space, classifier)
                analyze_score, score_per_color_list = DataHandler(test).analyze_with_classifier()

                # list to write in csv
                for polish_color in PolishColor:
                    data_for_csv = (
                        polish_color.name, pre_action.name, color_space.name, classifier.name,
                        analyze_score,
                        score_per_color_list[polish_color.get_polish_color_number()][1][0],
                        score_per_color_list[polish_color.get_polish_color_number()][1][1])
                    # write line to csv
                    writer.writerow(data_for_csv)

                # print to console
                print(f"Score in: {analyze_score}")
                print("Score out:", *[a for a in score_per_color_list], sep="\n")

    f.close()
"""
